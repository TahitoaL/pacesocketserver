from aiohttp import web
import socketio

sio = socketio.AsyncServer(cors_allowed_origins='*')
app = web.Application()
# instance
sio.attach(app)

# filter connections of robot
def connnectCheck(sid):
    return True

## can define aiohttp endpoints just as we normally
## would with no change
async def index(request):
    with open('index.html') as f:
        return web.Response(text=f.read(), content_type='text/html')


## If we wanted to create a new websocket endpoint,
## use this decorator, passing in the name of the
## event we wish to listen out for
@sio.on('action')
async def print_message(sid, message):
    ## When we receive a new event of type
    ## 'message' through a socket.io connection
    ## we print the socket ID and the message
    print("Socket ID: " , sid)
    print(message)


@sio.event
async def connect(sid, environ, auth):
    print(sid, 'connected')
    
    if connnectCheck(sid):
        print('valid')
        await sio.emit("validCon", "coucou")
    else:
        print('refused')
        await sio.emit("rejectCon")

@sio.event
def disconnect(sid):
    print(sid, 'disconnected')

# on peut remettre la télécommande web ici en cas de pépin
app.router.add_get('/', index)

## We kick off our server
if __name__ == '__main__':
    web.run_app(app)